import axios from 'axios'

const updateSolicitudService = {}

updateSolicitudService.updateSolicitud = function(peticion, id) {
  return axios({
    method: 'put',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/update-solicitud/'+id,
    data:  peticion
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... no se actualizaron los datos de la solicitud :( ' + err)
    )
}
export default updateSolicitudService
