import axios from 'axios'

const findEmisionService = {}

findEmisionService.findEmisionByIdSolicitud= function(idSolicitud) {
  return axios({
    method: 'get',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/find-emision-idSolicitud/'+idSolicitud,
    data:  ''
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... emision no en contrada :( ' + err)
    )
}
export default findEmisionService
