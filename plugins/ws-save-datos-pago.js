import axios from 'axios'

const emisionService = {}

emisionService.saveDatosPago = function(peticion) {
  return axios({
    method: 'post',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/saveDatosPago',
    data:  peticion
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... no pudo almacenar (datos del pago) :( ' + err)
    )
}
export default emisionService
