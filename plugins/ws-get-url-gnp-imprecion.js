import autosWs from './ws-autos'

const gnpimpresionService = {}

gnpimpresionService.search = function(numPoliza) {
    return autosWs
        .get('/gnp-impresion', {
            params: { numPoliza }
        })
        .then(res => res.data)
        .catch(err =>
            console.error('Ups... hubo un problema para obtener los Bancos ' + err)
        )
}
export default gnpimpresionService