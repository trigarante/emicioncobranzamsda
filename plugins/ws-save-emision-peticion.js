import axios from 'axios'

const emisionService = {}

emisionService.saveEmisionPeticion= function(peticion) {
  return axios({
    method: 'post',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/save-emision',
    data:  peticion
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... no guardaron los datos de la emision :( ' + err)
    )
}
export default emisionService
