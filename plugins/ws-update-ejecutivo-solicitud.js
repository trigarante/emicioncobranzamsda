import axios from 'axios'

const updateSolicitudService = {}

updateSolicitudService.updateEjecutivoSolicitud = function(id,idEjecutivo) {
  return axios({
    method: 'get',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/update-ejecutivo-solicitud/'+id+'/'+idEjecutivo,
    data:  ''
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... no se actualizaron los datos de la solicitud :( ' + err)
    )
}
export default updateSolicitudService
