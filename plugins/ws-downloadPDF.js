import baseWs from './ws-db'

const downloadDocumentService = {}

downloadDocumentService.downloadFilePDF = function(numPoliza,docs,aseguradora,passwordZip) {
  return baseWs
      .get('/descarga', {
        params: { numPoliza,docs, aseguradora,passwordZip}
      })
      .then(res => res.data)
      .catch(err =>
          console.error(
              'Ups... hubo un problema al descargar los documentos...' + err
          )
      )
}
export default downloadDocumentService
