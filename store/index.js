import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        state: {
            config: {
                isLoading: true,
                fullPage: true,
                urlRedirectError: 'https://ahorraseguros.mx/',
                clickButtonEmision: true,
                mostrarOne: true,
                DiaFechaNacimiento: "",
                MesFechaNacimiento: "",
                AnioFechaNacimiento: "",
                mostrarTwo: false,
                btnOculto: true,
                telefonoAS:'89655487',
                paso1: {
                    css: 'colorBgQualitas1'
                },
                paso2: {
                    css: 'colorBgQualitas1'
                },
                confirmarCotizacion: true,
                steps: {
                    step_uno: true,
                    sttribute_class_step_uno:'',
                    step_dos: false,
                    sttribute_class_step_dos: '',
                    step_tres: false,
                    sttribute_class_step_tres: '',
                    step_cuatro: false,
                    sttribute_class_step_cuatro: '',
                    step_cinco: false,
                    sttribute_class_step_cinco: ''
                },
                codigosPostales: [],
                colonias: [],
                bancos: [],
                urlPdf: '',
                viewButtonsPdf:false,
                alertErrors:{
                    error:{
                        titulo:'',
                        message:"",
                        comment:'',
                        thisErrors:true
                    },
                    errorSolicitud:{
                        titulo:'',
                        message:"",
                        comment:'',
                        thisErrors:true
                    }
                },
                datosEmisionTerminal:{
                    numSeguimiento:"",
                    numCotizacion:"",
                    numPoliza:"",
                    documento:""
                },
                configEspAseguradora:{
                    "AIG2":{
                        passwordZip:""
                    },
                    "AIG":{
                        passwordZip:""
                    }
                }
            },
            aseguradoras:{
                "ABA":{
                    nombre:"ABA",
                    id_aseguradora:1,
                    callback:"VN ABA",
                    DID:"5547440407",
                    emision:false,
                    useTerminalVirtual:true
                },
                "ANA":{
                    nombre:"ANA",
                    id_aseguradora:4,
                    callback:"VN ANA",
                    DID:"5547495119",
                    emision:false,
                    useTerminalVirtual:false
                },
                "ATLAS":{
                    nombre:"ATLAS",
                    id_aseguradora:5,
                    callback:"VN ATLAS",
                    DID:"5547495143",
                    emision:false,
                    useTerminalVirtual:false
                },
                "ATLAS2":{
                    nombre:"ATLAS2",
                    id_aseguradora:5,
                    callback:"VN ATLAS",
                    DID:"5547495143",
                    emision:false,
                    useTerminalVirtual:false
                },
                "AXA":{
                    nombre:"AXA",
                    id_aseguradora:6,
                    callback:"VN AXA",
                    DID:"5547495115",
                    emision:false,
                    useTerminalVirtual:false
                },
                "BANORTE":{
                    nombre:"BANORTE",
                    id_aseguradora:7,
                    callback:"VN BANORTE",
                    DID:"5547495120",
                    emision:false,
                    useTerminalVirtual:false
                },
                "GNP":{
                    nombre:"GNP",
                    id_aseguradora:11,
                    callback:"VN GNP",
                    DID:"5547495116",
                    emision:true,
                    useTerminalVirtual:true
                },
                "HDI":{
                    nombre:"HDI",
                    id_aseguradora:12,
                    callback:"VN HDI",
                    DID:"5547495127",
                    emision:false,
                    useTerminalVirtual:false
                },
                "QUALITAS":{
                    nombre:"QUALITAS",
                    id_aseguradora:18,
                    callback:"VN QUALITAS",
                    DID:"5547495114",
                    emision:false,
                    useTerminalVirtual:false
                },
                "MAPFRE":{
                    nombre:"MAPFRE",
                    id_aseguradora:15,
                    callback:"VN Mapfre",
                    DID:"5547495111",
                    emision:false,
                    useTerminalVirtual:false
                },
                "ELAGUILA":{
                    nombre:"ELAGUILA",
                    id_aseguradora:8,
                    callback:"VN EL AGUILA",
                    DID:"5547495125",
                    emision:true,
                    useTerminalVirtual:false
                },
                "SURA":{
                    nombre:"SURA",
                    id_aseguradora:33,
                    callback:"VN SURA",
                    DID:"5528814666",
                    emision:false,
                    useTerminalVirtual:false
                },
                "SURA2":{
                    nombre:"SURA",
                    id_aseguradora:33,
                    callback:"VN SURA",
                    DID:"5528814666",
                    emision:false,
                    useTerminalVirtual:false
                },
                "ZURICH":{
                    nombre:"ZURICH",
                    id_aseguradora:20,
                    callback:"VN ZURICH",
                    DID:"5547495130",
                    emision:false,
                    useTerminalVirtual:false
                },
                "AFIRME":{
                    nombre:"AFIRME",
                    id_aseguradora:2,
                    callback:"VN Multimarcas",
                    DID:"5547495142",
                    emision:true,
                    useTerminalVirtual:false
                },
                "AIG":{
                    nombre:"AIG",
                    id_aseguradora:3,
                    callback:"VN Multimarcas",
                    DID:"5528814666",
                    emision:false,
                    useTerminalVirtual:false
                },
                "AIG2":{
                    nombre:"AIG",
                    id_aseguradora:3,
                    callback:"VN Multimarcas",
                    DID:"5528814666",
                    emision:false,
                    useTerminalVirtual:false
                },
                "ELPOTOSI":{
                    nombre:"ELPOTOSI",
                    id_aseguradora:9,
                    callback:"VN Multimarcas",
                    DID:"5547495143",
                    emision:false,
                    useTerminalVirtual:false
                },
                "GENERALDESEGUROS":{
                    nombre:"GENERALDESEGUROS",
                    id_aseguradora:10,
                    callback:"VN Multimarcas",
                    DID:"47495143",
                    emision:true,
                    useTerminalVirtual:false
                },
                "INBURSA":{
                    nombre:"INBURSA",
                    id_aseguradora:13,
                    callback:"VN INBURSA",
                    DID:"5547495128",
                    emision:false,
                    useTerminalVirtual:false
                },
                "LALATINO":{
                    nombre:"LALATINO",
                    id_aseguradora:14,
                    callback:"VN Multimarcas",
                    DID:"5547495118",
                    emision:false,
                    useTerminalVirtual:false
                },
                "PRIMEROSEGUROS":{
                    nombre:"PRIMEROSEGUROS",
                    id_aseguradora:17,
                    callback:"VN Multimarcas",
                    DID:"5588801535",
                    emision:false,
                    useTerminalVirtual:false
                },
                "BXMAS":{
                    nombre:"BXMAS",
                    id_aseguradora:16,
                    callback:"VN Multimarcas",
                    DID:"5547440407",
                    emision:false,
                    useTerminalVirtual:false
                },
                "BXMAS2":{
                    nombre:"BXMAS",
                    id_aseguradora:16,
                    callback:"VN Multimarcas",
                    DID:"5547440407",
                    emision:false,
                    useTerminalVirtual:false
                }
            },
            datosCotizacion: {
                PrimaTotal: 0,
                idSolicitud: '',
                idAseguradora: '',
                detalle: '',
                precioCotizacion: '',
                detallesPorAseguradora: [],
                dataSolicitud: []
            },
            cotizacion: {},
            respuestaEmision: {},
            emision: {
                "Aseguradora": "",
                "Descuento": "0",
                "Cliente": {
                    "TipoPersona": "F",
                    "Nombre": "",
                    "ApellidoPat": "",
                    "ApellidoMat": "",
                    "RFC": "",
                    "FechaNacimiento": "",
                    "Ocupacion": null,
                    "CURP": null,
                    "Direccion": {
                        "Calle": "",
                        "NoExt": "",
                        "NoInt": "",
                        "Colonia": "",
                        "CodPostal": "",
                        "Poblacion": "",
                        "Ciudad": "",
                        "Pais": "MEXICO"
                    },
                    "Edad": "",
                    "Genero": "",
                    "Telefono": "",
                    "Email": ""
                },
                "Vehiculo": {
                    "Uso": "PARTICULAR",
                    "Marca": "",
                    "Modelo": "",
                    "NoMotor": "",
                    "NoSerie": "",
                    "NoPlacas": "",
                    "Descripcion": "",
                    "CodMarca": null,
                    "CodDescripcion": null,
                    "CodUso": null,
                    "Clave": "",
                    "Servicio": "PARTICULAR"
                },
                "Coberturas": [],
                "Paquete": "AMPLIA",
                "PeriodicidadDePago": 0,
                "Emision": {
                    "PrimaTotal": null,
                    "PrimaNeta": null,
                    "Derechos": null,
                    "Impuesto": null,
                    "Recargos": null,
                    "PrimerPago": null,
                    "PagosSubsecuentes": null,
                    "IDCotizacion": null,
                    "Terminal": null,
                    "Documento": null,
                    "Poliza": null,
                    "Resultado": null
                },
                "Pago":{
                    "MedioPago":"",
                    "NombreTarjeta":"",
                    "Banco":"",
                    "NoTarjeta":"",
                    "MesExp":"",
                    "AnioExp":"",
                    "CodigoSeguridad":"",
                    "NoClabe":null,
                    "Carrier":""
                },
                "CodigoError": null,
                "urlRedireccion": "https://poliza.ahorraseguros.mx/thankyouTerminal",
                "Cotizacion": {}
            },
            datosRegistroDb:{
                registro:{
                    "numPoliza":"",
                    "fechaInicio":"",
                    "idAseguradora":1,
                    "numSerie":"",
                    "numeroPlacas":"",
                    "numeroMotor":"",
                    "idTipoPago":1,
                    "primaNeta":0.00,
                    "primaTotal":0.00,
                    "seriePago":1,
                    "descuento":0,
                    "fechaProximoPago":null,
                    "archivo":"",
                    "clientePaterno":"",
                    "clienteMaterno":"",
                    "clienteNombre":"",
                    "clienteRfc":"",
                    "clienteCp":"",
                    "clienteTelefono":"",
                    "clienteCorreo":"",
                    "clienteTelefonoOp":"",
                    "clienteCorreoOp":"",
                    "fechaNac":"",
                    "clienteNacionalidad":"MEXICANA",
                    "idSolicitud":0,
                    "archivoINE":"",
                    "clienteEstado":null,
                    "clienteDelgMun":null,
                    "clienteCalle":null,
                    "clienteNumInt":null,
                    "clienteNumExt":null,
                    "clienteColonia":null,
                    "clienteGenero":"M",
                    "idEntidadFederativa":8,
                    "idMunicipio":234,
                    "registroAnterior":0,
                    "idEjecutivo":1,
                    "idEstado":1,
                    "idTipoPoliza":1,
                    "idestado_renovacion":4,
                    "rnumPoliza":null,
                    "rfechaInicio":null,
                    "ridAseguradora":0,
                    "renovacion":0,
                    "verificado":0,
                    "idCobertura":2,
                    "statusAsistencia":0,
                    "anio":"0000",
                    "marca":"",
                    "submarca":"",
                    "idMarcaCliente":0,
                    "idSubmarcaCliente":0,
                    "idDatosInspeccion":0,
                    "mnumPoliza":"NULL",
                    "mfechaInicio":null,
                    "midAseguradora":0,
                    "is_migracion":0,
                    "numeroOficina":null,
                    "corregido":0,
                    "renovaciontipobase":0,
                    "esEmision":0,
                    "banderace":0,
                    "registrada":0,
                    "is_reexpedicion":0,
                    "rxnumPoliza":"NULL",
                    "rxfechaInicio":null,
                    "rxidAseguradora":0,
                    "autorizada":1,
                    "es_gestion_cobranza":0,
                    "cnumPoliza":"NULL",
                    "cfechaInicio":null,
                    "is_cambioU":0,
                    "notas_gestion":"NULL",
                    "clnumPoliza":"NULL",
                    "clfechaInicio":null,
                    "clidAseguradora":0
                },
                datosPago:{
                    "referencia":"Cobrado por Team-Ultron",
                    "numAutorizacion":1,
                    "numeroDeTarjeta":"",
                    "vencimiento":"",
                    "titular":"",
                    "codigoSeg":"",
                    "carrier":"",
                    "meses":1,
                    "idMetodoPago":0,
                    "idBanco":15,
                    "bancoAbreviacion": "",
                    "idCarrier":0,
                    "idTipoTDC":""
                },
                pago:{
                    "cantidad":0,
                    "archivo":"",
                    "aplicado":0,
                    "aplicadoFnzs":0,
                    "fechaAplicado":"000-00-00",
                    "verificadoCobrado":0,
                    "verificadoEmitido":0,
                    "verificadoInspeccion":0,
                    "corregido":0,
                    "mesaControl":0,
                    "comentarios":"Emitida y cobrada desde web service",
                    "seriePago":1,
                    "aplicadoRen":0,
                    "fechaArchivo":null,
                    "idEjecutivo":1,
                    "idDatosPago":null,
                    "numPoliza":"",
                    "idAseguradora":0,
                    "idEstadoNomina":2,
                    "es_gestion_cobranza":0
                }
            }
        }
    })
}

export default createStore
